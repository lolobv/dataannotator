var express = require("express");
var bodyParser = require('body-parser');
var app = express();

var files = [];
var currentPath;
var mainPath, annotPath;
var serverImgPath;
var port = process.env.PORT || 8000;
var fs = require('fs-extra');
var path = require('path');

app.use(express.static(__dirname));
app.use('/data', express.static(path.join(__dirname + '/data')));
app.use(bodyParser.json());       // to support JSON-encoded bodies4
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
     
app.get("/", function(req, res) {
    res.render("home.html");
});


app.post("/setdir", function(req, res){
    mainPath = req.body.filePath;
	files=fs.readdirSync(mainPath).filter(function(value){ return value.slice(-3).localeCompare("jpg") === 0 || value.slice(-3).localeCompare("png") === 0 ;});
    mainPath = mainPath.split('\\').join('/');
    annotPath =  mainPath.slice(0,mainPath.lastIndexOf('/'))+'/annotations_json/';
    if(!fs.existsSync(annotPath)){
        fs.mkdirSync(annotPath);
    }
    
    var cpFolderName = mainPath.split("\\");
    currentPath =  'data/images/' + cpFolderName[cpFolderName.length-2];
    currentPath = currentPath.split("\\").join('/')
    fs.copySync(mainPath, currentPath);
    
    res.redirect("/annotate/"+0);
}); 


app.get("/annotate/:index", function(req, res) {
    var index = parseInt(req.params.index);
    res.render('app.html', {imgSrc : "../" + currentPath + '/' + files[index],
                            prevLink : ( index > 0 ? index-1 : 0 ),
                            nextLink : ( index < files.length-1 ? index+1 : files.length-1),      
    });
});
app.post("/savedata", function(req,res){
    
    
    var parsedData = [];
    req.body.forEach(function(val){
        parsedData.push(JSON.parse(val));
    });
    var fileName = annotPath + parsedData[0].imgSrc.slice(0,-4)+".json"
    console.log(fileName);
    fs.writeFile(fileName, JSON.stringify(parsedData));
    
});
app.listen(port, function() {
    console.log("Listening on port " + port);
});