var canvas = document.getElementById('cnv-1');
var c = canvas.getContext('2d');
var mouseDown = false;
var ctrlDown = false;
var x, y;
var rects = [];
var rIndex = 0;
document.addEventListener('keydown', function(e) {
	switch(e.keyCode){
		case 17:
			ctrlDown = true;
			break;
		case 8:
			e.preventDefault();
			break;
		case 37:
			rects[rIndex-1].translateX -= 1;
			rects[rIndex-1].x -= 1;
			rects[rIndex-1].x2 -= 1;
			break;
		case 38:
			rects[rIndex-1].translateY -= 1;
			rects[rIndex-1].y -= 1;
			rects[rIndex-1].y2 -= 1;
			e.preventDefault();
			break;
		case 39:
			rects[rIndex-1].translateX += 1;
			rects[rIndex-1].x += 1;
			rects[rIndex-1].x2 += 1;
			break;
		case 40:
			rects[rIndex-1].translateY += 1;
			rects[rIndex-1].y += 1;
			rects[rIndex-1].y2 += 1;
			e.preventDefault();		
		default:
			break;
		
	}
	draw();
});

document.addEventListener('keyup', function(e) {
	switch(e.keyCode){
		case 17:
			ctrlDown = false;
			break;
		case 8:
			e.preventDefault();
			removeLastRect();
			break;
		default:
			break;
	}
	draw();
});

canvas.addEventListener('mousedown', function(e) {
	mouseDown = true;

	rects.push({ 
		x: e.offsetX,
		y: e.offsetY,
		translateX:0,
		translateY:0,
		angle: 0
	});
});	

canvas.addEventListener('mousemove', function(e) {
	
	if (mouseDown) {		

		if (ctrlDown) {
			rects[rIndex].xg = rects[rIndex].x + Math.abs(rects[rIndex].x2 - rects[rIndex].x) / 2;
			rects[rIndex].yg = rects[rIndex].y + Math.abs(rects[rIndex].y2 - rects[rIndex].y) / 2;
			rects[rIndex].angle = Math.atan2(e.offsetY - rects[rIndex].yg, e.offsetX - rects[rIndex].xg);
		} else if (!rects[rIndex].angle) {									
			rects[rIndex].x2 = e.offsetX;
			rects[rIndex].y2 = e.offsetY;
		}
		draw();
	}
});

canvas.addEventListener('mouseup', function(e) {
	mouseDown = false;

	rects[rIndex].src = img.src;
	console.log( JSON.stringify(rects[rIndex]) );

	rIndex++;
});	

var img = new Image();

img.onload = function() {
	initCanvas();
	draw();
}


function removeLastRect(){
	if(rects.length > 0){
		rects.splice(rects.length-1,1);
		rIndex--;
		draw();
	}
}
function initCanvas() {
	canvas.width = img.width;
	canvas.height = img.height;
	c.strokeStyle = '#fff';
	c.fillStyle = 'rgba(255, 255, 255, 0.25)';
}

function draw() {
	c.drawImage(img, 0, 0, img.width, img.height);

	for (var i = 0; i < rects.length; ++i) {
		c.save();
		c.beginPath();

		c.translate(rects[i].xg, rects[i].yg);
		c.rotate(rects[i].angle);
		c.translate(-rects[i].xg, -rects[i].yg);
		var w = rects[i].x2 - rects[i].x;
		var h = rects[i].y2 - rects[i].y;

		c.strokeStyle = '#fff';
		c.rect(rects[i].x, rects[i].y, w, h);
		c.stroke();
		
		c.fill();
		c.closePath();

		c.beginPath();
		c.strokeStyle = '#f00';
		c.moveTo(rects[i].x, rects[i].y);
		c.lineTo(rects[i].x + w, rects[i].y);
		c.stroke();
		c.rotate(-rects[i].angle);
		c.closePath();

		c.restore();
	}
}