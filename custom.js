(function (window) {
    $("#saveButton").click(function(){
        var retArr = [];
        for(var i =0; i < rects.length; i++){
            var rectObj = {};
            rectObj.coords = []
            rectObj.coords.push(([rects[i].x - rects[i].translateX,rects[i].y- rects[i].translateY]));
            rectObj.coords.push(([rects[i].x2- rects[i].translateX,rects[i].y- rects[i].translateY]));
            rectObj.coords.push(([rects[i].x2- rects[i].translateX,rects[i].y2- rects[i].translateY]));
            rectObj.coords.push(([rects[i].x- rects[i].translateX,rects[i].y2- rects[i].translateY]));
            rectObj.angle = rects[i].angle;
            rectObj.translateX = rects[i].translateX;
            rectObj.translateY = rects[i].translateY;
            rectObj.imgSrc = rects[i].src.split("/").slice(-1)[0];
            retArr.push(JSON.stringify(rectObj));
        }
        
        $.ajax(
            {   type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "/savedata",
                //json object to sent to the authentication url
                data: JSON.stringify(retArr),
            }
            
            
        )
    });
   
})(this);